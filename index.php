<?php
include 'conex.php';
include 'DatabaseClass.php';
$database = new Database;

if( empty($_GET['month']) && empty($_GET['year']) ){
    $month = ltrim( date('m') , '0');
    $year = date('Y');
}else{
    $month = $_GET['month'];
    $year = $_GET['year'];
}

//Prepare values to insert
$vticket_id = str_replace('https://jira.scouting.org/browse/','',$_GET['ticket-url']);
$vticket_url = 'https://jira.scouting.org/browse/'.str_replace('https://jira.scouting.org/browse/','',$_GET['ticket-url']);
$vticket_name = $_GET['ticket-name'];
$vhours = $_GET['hours'];
$vdate = $_GET['date'];
    $datedata = explode("-",$_GET['date']);
$vday = ltrim( $datedata[2] , '0');
$vmonth = ltrim( $datedata[1] , '0');
$vyear = ltrim( $datedata[0] , '0');

//Assign values to insert
$ticket_id = $vticket_id;
$ticket_url = $vticket_url;
$ticket_name = $vticket_name;
$hours = $vhours;
$date = $vdate;
$day = $vday;
$month = $vmonth;
$year = $vyear;

if( !empty($_GET['insert']) && !empty($_GET['ticket-url']) ){
    $database->query("INSERT INTO history (ticket_id, ticket_url, ticket_name, hours, date, day, month, year) VALUES ('$ticket_id', '$ticket_url', '$ticket_name', '$hours', '$date', '$day', '$month', '$year')");
    $database->execute();
    header("location:.");
}
if( !empty($_GET['edit']) && !empty($_GET['ticket-url']) ){
    $database->query("UPDATE history SET ticket_id='$ticket_id', ticket_url='$ticket_url', ticket_name='$ticket_name', hours='$hours', date='$date', day='$day', month='$month', year='$year' WHERE id='".$_GET['id']."'");
    $database->execute();
    header("location:.");
}
if( !empty($_GET['delete']) && !empty($_GET['id']) ){
    $database->query("DELETE FROM history WHERE id='".$_GET['id']."'");
    $database->execute();
    header("location:.");
}
?>


<?php
include 'showmonth.php'; 
if( !isset($_GET['month']) ){
    $month = ltrim( date('m') , '0');
}else{
    $month = $_GET['month'];
}

if( !isset($_GET['year']) ){
    $year = date('Y');
}else{
    $year = $_GET['year'];
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>⏰ Track Hack - Time Tracking</title>
    
    <script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

    <link rel="stylesheet" href="datepicker/dist/datepicker.min.css">
    <script src="datepicker/dist/datepicker.min.js"></script>
    <script>
        $(function(){
            $('#datepicker').datepicker({
                format: 'yyyy-mm-dd'
            });
        });
    </script>

    <style>
        #calendar{

        }
        #calendar tr{

        }
        #calendar tr td, #calendar tr th{
            padding: 10px 10px;
            text-align: center;
        }
    </style>

</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-md-12 mt-4">
                <h2><a href=".">Track Hack</a></h2>
            </div>
        </div>
    </div>
    <hr class="mb-0">
    <div class="container">
        <div class="row">
            
                <div class="col-md-6 mt-4">
                    <form action="." method="GET">
                        <div class="mb-4">
                            <select class="form-control mr-2" style="width: 100px; float: left;" name="month">
                                <option <?php if(!isset($_GET['month']) && date("m")=='1'){ echo 'selected'; }else{ if( $_GET['month']=='1' ){ echo 'selected'; } } ?> value="1">Enero</option>
                                <option <?php if(!isset($_GET['month']) && date("m")=='2'){ echo 'selected'; }else{ if( $_GET['month']=='2' ){ echo 'selected'; } } ?> value="2">Febrero</option>
                                <option <?php if(!isset($_GET['month']) && date("m")=='3'){ echo 'selected'; }else{ if( $_GET['month']=='3' ){ echo 'selected'; } } ?> value="3">Marzo</option>
                                <option <?php if(!isset($_GET['month']) && date("m")=='4'){ echo 'selected'; }else{ if( $_GET['month']=='4' ){ echo 'selected'; } } ?> value="4">Abril</option>
                                <option <?php if(!isset($_GET['month']) && date("m")=='5'){ echo 'selected'; }else{ if( $_GET['month']=='5' ){ echo 'selected'; } } ?> value="5">Mayo</option>
                                <option <?php if(!isset($_GET['month']) && date("m")=='6'){ echo 'selected'; }else{ if( $_GET['month']=='6' ){ echo 'selected'; } } ?> value="6">Junio</option>
                                <option <?php if(!isset($_GET['month']) && date("m")=='7'){ echo 'selected'; }else{ if( $_GET['month']=='7' ){ echo 'selected'; } } ?> value="7">Julio</option>
                                <option <?php if(!isset($_GET['month']) && date("m")=='8'){ echo 'selected'; }else{ if( $_GET['month']=='8' ){ echo 'selected'; } } ?> value="8">Agosto</option>
                                <option <?php if(!isset($_GET['month']) && date("m")=='9'){ echo 'selected'; }else{ if( $_GET['month']=='9' ){ echo 'selected'; } } ?> value="9">Septiembre</option>
                                <option <?php if(!isset($_GET['month']) && date("m")=='10'){ echo 'selected'; }else{ if( $_GET['month']=='10' ){ echo 'selected'; } } ?> value="10">Octubre</option>
                                <option <?php if(!isset($_GET['month']) && date("m")=='11'){ echo 'selected'; }else{ if( $_GET['month']=='11' ){ echo 'selected'; } } ?> value="11">Noviembre</option>
                                <option <?php if(!isset($_GET['month']) && date("m")=='12'){ echo 'selected'; }else{ if( $_GET['month']=='12' ){ echo 'selected'; } } ?> value="12">Diciembre</option>
                            </select>

                            <select class="form-control mr-2" style="width: 100px; float: left;" name="year">
                                <?php
                                $years_behind = 10;
                                $current_year = date('Y');
                                for ($i=0; $i < $years_behind; $i++) { ?>
                                    <option value="<?php echo $current_year-$i; ?>" <?php if(!isset($_GET['year']) && date("m")==$current_year){ echo 'selected'; }else{ if( $_GET['year']==$current_year-$i ){ echo 'selected'; } } ?> value="12"><?php echo $current_year-$i; ?></option>
                                <?php } ?>
                            </select>

                            <button class="btn btn-info">
                                Cambiar
                            </button>
                        </div>
                    </form>
                    
                    <?php echo showMonth($month, $year, $database); ?>

                    <div class="my-3 mt-4 bg-white rounded box-shadow">
                        <h6 class="border-bottom border-gray pb-2 mb-0">Historial</h6>
                        
                        <?php $database->query("SELECT * FROM history WHERE month='$month' AND year='$year' ORDER BY date DESC");
                        foreach($database->getAll() as $row){?>
                        <div class="media text-muted pt-3">
                            <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                            <strong class="d-block text-gray-dark"><?=$row['ticket_id']?> (<?=$row['date']?>) <?=$row['hours']?> horas</strong>
                            <?=stripslashes($row['ticket_name'])?><br>
                            <!--<a target="_blank" href="<?=$row['ticket_url']?>"><?=$row['ticket_url']?></a>-->
                            <a href="#" onClick="MyWindow=window.open('<?=$row['ticket_url']?>','MyWindow',width=600,height=300); return false;"><?=$row['ticket_url']?></a>
                            </p>

                            <small class="d-block text-right mt-3">
                                <a class="btn btn-info btn-sm" href="?edit=1&id=<?=$row['id']?>">Editar</a> &nbsp; 
                                <a onclick="javascript: if( confirm('¿Eliminar?') ){ window.location.href='index.php?delete=1&id=<?=$row['id']?>'; }" class="btn btn-danger btn-sm" href="javascript:void(0)">Eliminar</a>
                            </small>
                        </div>
                        <?php }?>

                    </div>

                </div>
            
            
                <?php if(!empty($_GET['edit'])){
                    $database->query("SELECT * FROM history WHERE id='".$_GET['id']."'");
                    $datos = $database->getOne();
                } ?>

                <div class="col-md-6 mt-4 mb-4">
                    <form action="." method="GET">
                        <?php if(!empty($_GET['edit'])){?>
                            <input type="hidden" name="edit" value="1">
                            <input type="hidden" name="id" value="<?=$_GET['id']?>">
                        <?php }else{ ?>
                            <input type="hidden" name="insert" value="1">
                        <?php } ?>
                        <h3>Capturar horas</h3>
                            <!--<p><small>Nota: Oficialmente debes trabajar 7 horas al día (no se cuenta tu hora de comida)</small></p>-->
                            <input autocomplete="off" value="<?=$datos['ticket_url']?>" name="ticket-url" placeholder="Ticket URL o Ticket ID (Ejemplo: WEB-2226)" type="text" class="form-control mb-3  ">
                            <input autocomplete="off" value="<?=$datos['ticket_name']?>" name="ticket-name" placeholder="Nombre del ticket" type="text" class="form-control">
                            <div class="row mt-3">
                                <div class="col-md-6 mb-4">
                                    <input autocomplete="off" value="<?=$datos['hours']?>" type="number" name="hours" placeholder="Horas dedicadas" class="form-control">
                                </div>
                                <div class="col-md-6 mb-4">
                                    <input autocomplete="off" value="<?=$datos['date']?>" id="datepicker" type="text" name="date" placeholder="Fecha" class="form-control">
                                </div>
                            </div>
                            <button class="form-control btn btn-info">Guardar</button>
                        </form>
                </div>
        </div>
    </div>

</body>
</html>